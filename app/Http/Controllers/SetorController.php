<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;

class SetorController extends Controller
{
    private $suratjalan;
    private $cekbatal_sj;

    public function __construct()
    {
        date_default_timezone_set("Asia/jakarta");

        $this->suratjalan = DB::table('suratjalan')
                                ->where('is_cek_nota', 1)
                                ->get();

        $this->cekbatal_sj = DB::table('suratjalan')
                                ->whereNotNull('is_batal')
                                ->get();

        $this->batal_sj = DB::table('suratjalan')
                                ->WhereNotNull('is_batal')
                                ->select('id','batal')
                                ->get();
    }

    public function cek_sj($suratjalan, $id)
    {
        $suratjalan = DB::table('suratjalan')
                            ->where('is_cek_nota', 1)
                            ->where('id', $id)
                            ->first();

        return isset($suratjalan) ? 'y' : 'n';
    }

    public function cek_batal_sj($cekbatal, $id)
    {
        $dty = array_filter($cekbatal->toArray(), function ($v) use ($id) {
            return $v->id == $id;
        });

        return !empty($dty) ? 'y' : 'n';
    }

    public function batal_sj($batal, $id)
    {
        $dty = array_filter($batal->toArray(), function ($v) use ($id) {
            return $v->id == $id;
        });

        $dt = [];
        if (!empty($dty)) {
            foreach ($dty as $a) {
                $dt = $a->batal;
            }    
        }else {
            $dt = '';
        }
        return $dt;
    }

    public function index()
    {
        $saldo = 0;

        $data = DB::table('jurnal')
                            ->where('status', NULL)
                            ->where('tgl', date('Y-m-d'));
        
        $dt = [];
        $total_penjualan = 0;
        $total_hpp = 0;
        foreach ($data->get() as $key => $v) {
            $is_sj = $this->cek_sj($this->suratjalan, $v->ref);
            $cek_batal_sj = $this->cek_batal_sj($this->cekbatal_sj, $v->ref);
            $batal_sj = $this->batal_sj($this->batal_sj, $v->ref);

            if (($v->jenis_jurnal == 'sj') && ($is_sj == 'y')) {
                $dt[] = (object) [
                    'id' => $v->id, 
                    'tgl' => $v->tgl,
                    'jurnal' => $v->jurnal,
                    'id_item' => $v->id_item,
                    'no_akun' => $v->no_akun,
                    'hpp' => $v->hpp,
                    'grup' => $v->grup,
                    'jenis_jurnal' => $v->jenis_jurnal,
                    'ref' => 'ina '.$v->ref,
                    'bk' => $v->bk,
                    'nama' => ($cek_batal_sj == 'y') ? 'BATAL' : strtolower($v->nama),
                    'keterangan' => ($cek_batal_sj == 'y') ? $batal_sj : $v->keterangan,
                    'map' => ($cek_batal_sj == 'y') ? '' : $v->map,
                    'hit' => ($cek_batal_sj == 'y') ? '' : $v->hit,
                    'qty' => ($cek_batal_sj == 'y') ? '' : $v->qty,
                    'm3' => ($cek_batal_sj == 'y') ? '' : $v->m3,
                    'harga' => ($cek_batal_sj == 'y') ? '' : $v->harga,
                    'total' => ($cek_batal_sj == 'y') ? '' : $v->total,
                    'status' => ($cek_batal_sj == 'y') ? '' : $v->status,
                    'batal' => $cek_batal_sj
                ];
            }  
        }
               
        dd($dt);
                        
        // $sum_kwitansi_masuk = DB::table('kwitansi')
        //                         ->where('tgl', date('Y-m-d'))
        //                         ->whereNotNull('is_cek_kwi')
        //                         ->where('jenis_kwitansi', 'pemasukan')
        //                         ->sum('total');
        
        // $sum_kwitansi_keluar = DB::table('kwitansi')
        //                         ->where('tgl', date('Y-m-d'))
        //                         ->whereNotNull('is_cek_kwi')
        //                         ->where('jenis_kwitansi', 'pengeluaran')
        //                         ->sum('total');

        // $sum_pembelian = DB::table('beli')
        //                         ->where('tgl', date('Y-m-d'))
        //                         ->whereNotNull('is_cek_beli')
        //                         ->sum('total');  

        // $saldo = ($sum_penjualan + $sum_kwitansi_masuk) - ($sum_kwitansi_keluar + $sum_pembelian);
        $data['saldo'] = number_format($sum_penjualan, 0, ',', '.');

        return view('admin.setor.index')->with($data);
    }

    public function simpan_setor(Request $req)
    {
        $id_user = Session::get('id_user');
        $tgl = date('Y-m-d', strtotime($req->_tgl));
        $saldo = $req->_saldo;

        $data_setor = [
            'tgl'           => $tgl,
            'saldo'         => $saldo,
            'created_at'    => date('Y-m-d H:i:s'),
            'user_add'      => $id_user
        ];
        
        DB::beginTransaction();

        try {
            $cek = DB::table('setor')->where('tgl', $tgl)->first();

            if (isset($cek)) {
                $res = [
                    'code'  => 400,
                    'msg'   => 'Uang Sudah disetor'       
                ];
            } else {
                $insert_setor = DB::table('setor')->insert($data_setor);
                DB::commit();

                $res= [
                    'code'  => 300,
                    'msg'   => 'Uang Sudah di Setor'
                ];
            }
        } catch (\Throwable $th) {
            DB::rollback();

            $res = [
                'code' => 400,
                'msg' => $th->getMessage()
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }

    public function datatable()
    {
        $data = DB::table('setor')->get();

        return datatables::of($data) 
        ->addIndexColumn()
        ->make(true);
    }
}
