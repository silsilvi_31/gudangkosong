<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;

class ListAkunController extends Controller
{
    public function __construct ()
    {
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        return view('admin.ListAkun.index');
    }


    public function sum_akun_debit($no_akun, $jurnal)
    {
        $array_data = array_filter($jurnal->toArray(), function ($value) use ($no_akun) {
            return $value->no_akun == $no_akun && $value->map == 'd';            
        });

        $qty = 0;
        $total = 0;

        if (isset($array_data)) {
            foreach ($array_data as $v) {
                if ($v->hit == 'b') {
                    $qty += $v->qty;
                    $total += $v->qty * $v->harga;
                }else {
                    $qty += $v->qty;
                    $total += $v->harga;
                }
            }
        }
        $data = [
            'qty' => $qty,
            'total' => $total
        ];
        return $data;
    }

    public function sum_akun_kredit($no_akun, $jurnal)
    {
        $array_data = array_filter($jurnal->toArray(), function ($value) use ($no_akun) {
            return $value->no_akun == $no_akun && $value->map == 'k';            
        });

        $qty = 0;
        $total = 0;

        if (isset($array_data)) {
            foreach ($array_data as $v) {
                if ($v->hit == 'b') {
                    $qty += $v->qty;
                    $total += $v->qty * $v->harga;
                }else {
                    $qty += $v->qty;
                    $total += $v->harga;
                }
            }
        }
        $data = [
                    'qty' => $qty,
                    'total' => $total
                ];
        return $data;
    }

    public function list_akun_json(Request $req)
    {
        $tgl = isset($req->_tgl) ? date("Y-m-d", strtotime($req->_tgl)) : null;

        $jurnal = DB::table('jurnal')
                    ->where('ref', 2023)
                    // ->where('no_akun', $no_akun)
                    // ->where('map', 'k')
                    ->where('tgl', $tgl)
                    ->get();

        $akun = DB::table('akun')
                    ->where('locked', null)
                    // ->where('no_akun', '1501,02')
                    ->get();
        
        $list_akun = [];
        foreach ($akun as $v) {
            $list_akun[] = (object)[
                'no_akun' => $v->no_akun,
                'akun' => $v->akun,
                'qty' => $this->sum_akun_debit($v->no_akun, $jurnal)['qty'] - $this->sum_akun_kredit($v->no_akun, $jurnal)['qty'],
                'total' => $this->sum_akun_debit($v->no_akun, $jurnal)['total'] - $this->sum_akun_kredit($v->no_akun, $jurnal)['total']
            ];
        }
        
        $data['akun'] = $list_akun;
        return response()->json($data);
    }
}