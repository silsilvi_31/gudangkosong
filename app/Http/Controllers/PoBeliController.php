<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;
use File;
use Excel;
use Exception;

class PoBeliController extends Controller
{
    private $transaksi_po;

    public function __construct()
    {
        date_default_timezone_set("Asia/Jakarta");
        $this->transaksi_po = DB::table('transaksi_po_beli')->where('is_po', 1)->get();
    }

    public function get_karyawan($id_user)
    {
        $data = DB::table('karyawan')->where('id_users', $id_user)->first();
        return isset($data) ? $data->nama : 'No Name';
    }


    public function index()
    {
        return view('admin.poBeli.index');
    }

    public function form()
    {
        $satuan = DB::table('satuan')->get();
        $jenis_brg = DB::table('jenis_barang')->get();
        $akun = DB::table('akun')->get();
        $suplier = DB::table('suplier')->get();

        $data['satuan'] = $satuan;
        $data['jenis_barang'] = $jenis_brg;
        $data['akun'] = $akun;
        $data['suplier'] = $suplier;
        
        return view('admin.poBeli.form')->with($data);
    }

    public function no_urut_po()
    {
        $id_beli_po = DB::table('beli_po')->max('id_beli');
        $no = $id_beli_po;
        $no++;
        return response()->json($no);
    }
    
    public function hitung_total(Request $req)
    {
        $id = $req->_id;
        $beli = DB::table('beli_po')->where('id_beli', $id)->first();
        $total = isset($beli) ? $beli->total : NULL;
        $data['total'] = number_format($total, 0, ',', '.');

        return response()->json($data);
    }

    public function get_qty_beli($data_beli, $id_brg, $tgl)
    {
        $cari = [
                    'id_brg' => (string) $id_brg,
                    'tgl' => (string) $tgl
        ];

        $dty = array_filter($data_beli, function ($value) use ($cari) {
            return $value['id_brg'] == $cari['id_brg'] && $value['tgl'] >= $cari['tgl'];
        });

        $tmp = [];
        $hasil = 0;
        foreach ($dty as $k => $v) {
            $hasil += $v['qty'];
        }

        return $hasil;
    }

    public function get_brg_jual($sj_detail, $id_brg, $tgl)
    {
        $cari = [
            'id' => $id_brg,
            'tgl_time' => $tgl
        ];
        
        $dty = array_filter($sj_detail, function ($value) use ($cari) {
            return $value['id_brg'] == $cari['id'] && $value['tgl'] >= $cari['tgl_time'];
        });

        if (is_null($tgl)) {
            $dty = array_filter($sj_detail, function ($value) use ($cari) {
                return $value['id_brg'] == $cari['id'];
            });
        }

        $hasil = 0;
        if (isset($dty)) {
            foreach ($dty as $v) {
                $hasil += $v['qty'];
            }
        }
        return $hasil;
    }

    public function get_brg_masuk($apiIna, $id_brg, $tgl)
    {
        $id = $id_brg;
        $cari = [
            'id' => $id_brg,
            'tgl_time' => $tgl
        ];

        $dty = array_filter($apiIna, function ($value) use ($cari) {
            return $value['id_brg'] == $cari['id'] && $value['tgl'] >= $cari['tgl_time'];
        });

        if (is_null($tgl)) {
            $dty = array_filter($apiIna, function ($value) use ($cari) {
                return $value['id_brg'] == $cari['id'];
            });
        }

        $hasil = 0;
        if (isset($dty)) {
            foreach ($dty as $v) {
                $hasil += $v['qty'];
            }
        }
        
        return $hasil;
    }

    public function get_transaksi($transaksi, $id)
    {
        $dty = array_filter($transaksi->toArray(), function ($value) use ($id) {
            return $value->id_beli == $id;
        });

        $bayar = null;
        foreach ($dty as $value) {
            $bayar += $value->total;
        }

        $data['bayar'] = $bayar;
        return $data;
    }

    public function datatable()
    {
        $data = DB::table('beli_po as a')
                        // ->where('a.status',5)
                        // ->leftJoin('karyawan as kar', 'a.id_karyawan', '=', 'kar.kode')
                        ->leftJoin('suplier as spl', 'a.suplier', '=', 'spl.id')
                        ->select('a.id', 'a.id_beli','a.tgl', 'a.total','a.user_batal', 'a.ket_batal',
                                    'spl.nama')
                        ->get(); 

        $transaksi = $this->transaksi_po;

        return datatables::of($data)
        ->addColumn('total_bayar', function ($data)  use ($transaksi){
            $total_bayar = $this->get_transaksi($transaksi, $data->id)['bayar'];
            return $total_bayar;
        })
        ->addColumn('sisa_tagihan', function ($data) use ($transaksi) {
            $total = $data->total;
            $total_bayar = $this->get_transaksi($transaksi, $data->id)['bayar'];
            $sisa_tagihan = $total - $total_bayar;
            return ($sisa_tagihan <= 0) ? '<span class="badge badge-success">Lunas</span>' : $sisa_tagihan;
        })
        ->addColumn('status', function ($data) {
            return 'PO';
        })
        ->editColumn('cek_batal', function ($data) {
            $user_btl = isset($data->user_batal) ? ' ('.$this->get_karyawan($data->user_batal). ')' : null;
            $ket_btl = isset($data->user_batal) ? $data->ket_batal : null;
            return $ket_btl.$user_btl;
        })
        ->addColumn('opsi',  function ($data) use ($transaksi) {
            $total = $data->total;
            $total_bayar = $this->get_transaksi($transaksi, $data->id)['bayar'];

            $status_lunas = ( ($total <= $total_bayar) || (isset($data->user_batal)) ) ? 'btn-secondary' : 'btn-primary' ;
            $status_lns = ( ($total <= $total_bayar) || (isset($data->user_batal)) ) ? 'disabled' : '' ;
            $status_tampil = ( ($total <= $total_bayar) || (isset($data->user_batal)) ) ? 'none' : '';
            return '<button type="button" class="btn btn-sm '.$status_lunas.'" '.$status_lns.' data-toggle="modal" data-target="#modal_pelunasan" data-id="'.$data->id.'" data-idbeli="'.$data->id_beli.'">Pelunasan</button>
                    <button type="button" class="btn btn-sm btn-danger" style="display:'.$status_tampil.'" onclick="delete_po('.$data->id_beli.')"><i class="fa fa-trash"></i></button>
                    <button type="button" class="btn btn-sm btn-danger" style="display:'.$status_tampil.'" data-toggle="modal" data-target="#modal_btl_po" data-id="'.$data->id.'" data-idbeli="'.$data->id_beli.'">Batal</button>';
        })
        ->rawColumns(['opsi', 'sisa_tagihan'])
        ->make(true);
    }

    public function datatable_brg()
    {
        $tgl_now = date("Y-m-d");
        $data = DB::table('barang as a')
                ->where('a.status',NULL)
                ->where('jenis_brg', 5)
                ->leftJoin('satuan as b', 'a.satuan', '=', 'b.id')
                ->select('a.kode', 'a.nama_brg', 'a.hpp', 'a.harga', 'a.satuan as id_satuan','b.nama as satuan', 'a.jenis_brg','a.stok','a.tgl_opname', 'a.no_akun')
                ->get();
        
        // $rUrl =  $this->url;
        // $apiIna = json_decode(file_get_contents($rUrl), true);

        $brg_sj = [];
        // $brg_masuk = [];
        $brg_beli = [];
        $brg_retur_jual = [];
        $brg_retur_beli = [];

        // foreach ($apiIna as $y => $e) {
        //     $brg_masuk[] = [
        //         'id_sj' => $e['id_sj'],
        //         'tgl' => (string)strtotime($e['tgl']),
        //         'tgl_s' => $e['tgl'],
        //         'id_brg' => $e['id_brg'],
        //         'nama_brg' => $e['nama_brg'],
        //         'qty' => $e['qty'],
        //         'harga' => $e['harga'],
        //         'harga_new' => $e['harga_new']
        //     ];
        // }

        $sj_detail = DB::table('suratjalan_detail as a')
                                ->leftJoin('suratjalan as b', 'a.id_sj', '=', 'b.id')
                                // ->whereNotNull('b.bayar')
                                ->whereNotNull('b.is_cek_nota')
                                ->select('a.id_brg','a.qty','b.tgl')
                                ->get();

        foreach ($sj_detail as $t => $d) {
            $brg_sj[] = [
                'id_brg' => $d->id_brg,
                'qty' => $d->qty,
                'tgl' => strtotime($d->tgl)
            ];
        }

        $beli = DB::table('beli as a')
                            ->leftJoin('beli_detail as b', 'a.id_beli', '=', 'id_detail_beli')
                            ->whereNotNull('a.is_cek_beli')
                            ->whereNull('a.user_batal')
                            ->get();

        foreach ($beli as $key => $value) {
            $brg_beli[] = [
                'id_brg' => $value->id_brg,
                'qty' => $value->qty,
                'tgl' => strtotime($value->tgl)
            ];
        }

        $retur_jual = DB::table('retur_brg')
                        ->where('jenis', 'jual')
                        ->get();

        foreach ($retur_jual as $y) {
            $brg_retur_jual[] = [
                'id_brg'   => $y->id_brg,
                'qty'      => $y->qty_retur,
                'tgl'      => strtotime($y->tgl_retur)
            ];  
        }

        $retur_beli = DB::table('retur_brg')
                            ->where('jenis', 'beli')
                            ->get();

        foreach ($retur_beli as $y) {
            $brg_retur_beli[] = [
                'id_brg'   => $y->id_brg,
                'qty'      => $y->qty_retur,
                'tgl'      => strtotime($y->tgl_retur)
            ];  
        }

        $dt = [];
        $awal = 0;
        $hasil = 0;
        $hasil_masuk = 0;

        foreach ($data as $v) {
            $opname_time = isset($v->tgl_opname) ? (string)strtotime($v->tgl_opname) : null; 

            $stok_b = $this->get_qty_beli($brg_beli, $v->kode, $opname_time);
            $stok_k = $this->get_brg_jual($brg_sj, $v->kode, $opname_time);
            // $stok_m = $this->get_brg_masuk($brg_masuk, $v->kode, $opname_time) + $stok_b;
            $stok_r_jual = $this->get_brg_masuk($brg_retur_jual, $v->kode, $opname_time);
            $stok_r_beli = $this->get_brg_masuk($brg_retur_beli, $v->kode, $opname_time);

            $hasil = $v->stok + ($stok_b + $stok_r_jual) - ($stok_k + $stok_r_beli);

            $dt[] = (object) [
                'kode' => $v->kode,
                'nama_brg' => $v->nama_brg,
                'no_akun' => $v->no_akun,
                'hpp' => $v->hpp,
                'harga' => $v->harga,
                'id_satuan' => $v->id_satuan,
                'satuan' => $v->satuan,
                'stok_awal' => $awal,
                'stok_masuk' => $v->stok + $stok_b + $stok_r_jual,
                'stok_keluar' => $stok_k + $stok_r_beli,
                'sisa' => $hasil,
                'tgl_opname' => $opname_time,
                'list---------' => $stok_b,
                'list_keluar' => $stok_k
            ];
        }

        // dd($dt);
        return Datatables::of($dt)
        ->addIndexColumn()
        ->addColumn('opsi', function ($dt){
            $kode_barang = $dt->kode;
            $nama_brg = "'".$dt->nama_brg."'";
            $hpp = $dt->hpp;
            $harga = $dt->harga;
            $id_satuan = $dt->id_satuan;
            $satuan = "'".$dt->satuan."'";
            $sisa = $dt->sisa;
            $no_akun = $dt->no_akun;
            return '<button id="btn_pilih" type="button" class="btn btn-sm btn-primary" onclick="select_brg('.$kode_barang.','.$nama_brg.','.$hpp.','.$harga.','.$id_satuan.','.$satuan.','.$sisa.', '.$no_akun.')">Pilih</button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function save(Request $req)
    {
        $id_user = session::get('id_user');
        $id_beli_po = $req->_idBeliPo;
        $tgl = $req->_tgl;
        $id_suplier = $req->_idSuplier;
        $no_nota_spl = $req->_noNotaSpl;
        $no_akun_k = $req->_noAkunK;
        $opsi = $req->_opsi;
        $dp = $req->_dp;

        $id_item = $req->_idItem;
        $id_brg = $req->_idBrg;
        $nama_brg = $req->_namaBrg;
        $no_akun_d = $req->_noAkunD;
        $id_satuan = $req->_idSatuan; 
        $qty = $req->_qty;
        $hpp = $req->_hpp;
        $harga = $req->_harga;
        $subtotal = $req->_subtotal;
        $ketr = $req->_ketr;
        
        $beli_po = [
                    'id_beli' => $id_beli_po,
                    'tgl' => date('Y-m-d', strtotime($tgl)),
                    'suplier' => $id_suplier,
                    'no_nota_spl' => $no_nota_spl,
                    'no_akun' => $no_akun_k,
                    'opsi' => $opsi,
                    "created_at" => date("Y-m-d H:i:s"),
                    "user_add" => $id_user
                ];

        $detail_beli_po = [
                            // 'id_detail_beli' => $id_beli_po,
                            'id_brg' => $id_brg,
                            'nama_brg' => $nama_brg,
                            'no_akun_brg' => $no_akun_d,
                            'ketr' => $ketr,
                            'id_satuan' => $id_satuan,
                            'qty' => $qty,
                            'harga' => $hpp,
                            'subtotal' => $subtotal
                        ];
                        
        $cek_beli_po = DB::table('beli_po')->where('id_beli',$id_beli_po)->first();
        
        $parent_jurnal = DB::table('parent_jurnal')
                                        ->where('status', 'tutup')
                                        ->orderBy('created_at', 'DESC')
                                        ->first();

        $tgl_int = strtotime($tgl);
        $tgl_akhir_int = isset($parent_jurnal) ? strtotime($parent_jurnal->tgl_akhir) : null;
        // dd($tgl_int);
        if (!$tgl || !$id_suplier || !$no_nota_spl) {
            $res = [
                'code' => 400,
                'msg' => 'Data Belum Lengkap'
            ];
        } elseif ($tgl_int <= $tgl_akhir_int) {
            $res = [
                'code' => 400,
                'msg' => 'Tgl tidak sesuai'
            ];
        } else {
            if (is_null($cek_beli_po)) {
                 $insert_beli_po = DB::table('beli_po')->insertGetId($beli_po);
                if($insert_beli_po) {
                    $detail_beli_po['id_detail_beli'] = $insert_beli_po;
                    $insert_detail_beli_po = DB::table('beli_detail_po')->insertGetId($detail_beli_po);
                    $jumlah = DB::table('beli_detail_po')->where('id_detail_beli',$insert_beli_po)->where('status',NULL)->sum('subtotal');
                    $update_total_beli = DB::table('beli_po')->where('id_beli', $id_beli_po)->update([
                        'total' => $jumlah
                    ]);
                    $res = [
                        'code' => 300,
                        'msg' => 'Data Berhasil disimpan'
                    ];
                } else {
                    $res = [
                        'code' => 400,
                        'msg' => 'Data Gagal disimpan'
                    ];
                }
            } elseif (!is_null($cek_beli_po)) {
                if (isset($id_item)) {
                    $beli_detail_po = DB::table('beli_detail_po')->where('id', $id_item)->first();

                    $update_item = DB::table('beli_detail_po')->where('id',$id_item)->update($detail_beli_po);

                    if ($update_item) {
                        $jumlah = DB::table('beli_detail_po')->where('id_beli',$id_beli_po)->where('status',NULL)->sum('subtotal');
                        $update_total_beli = DB::table('beli_po')->where('id_beli', $id_beli_po)->update([
                            'total' => $jumlah,
                        ]);
                        $res = [
                            'code' => 200,
                            'msg' => 'Data Barang Berhasil Diupdate',
                        ];
                    } else {
                        $res = [
                            'code' => 400,
                            'msg' => 'Data Barang Gagal Diupdate'
                        ];
                    }
                } else {
                    if (isset($nama_brg)) {
                        $dt_beli = DB::table('beli_po')->where('id_beli', $id_beli_po)->first();
                        $detail_beli_po['id_detail_beli'] = $dt_beli->id;
                        $insert_beli_detail = DB::table('beli_detail_po')->insertGetId($detail_beli_po);
                        if (isset($insert_beli_detail)) {
                            $jumlah = DB::table('beli_detail_po')->where('id_detail_beli',$dt_beli->id)->where('status',NULL)->sum('subtotal');
                            $update_total_beli = DB::table('beli_po')->where('id', $dt_beli->id)->update([
                                'total' => $jumlah,
                            ]);
                            $res = [
                                'code' => 200,
                                'msg' => 'Data Barang Berhasil Disimpan',
                            ];
                        } else {
                            $res = [
                                'code' => 400,
                                'msg' => 'Data Barang Gagal Disimpan'
                            ];
                        }
                    }
                    $update_beli =  DB::table('beli_po')->where('id_beli', $id_beli_po)->update($beli_po);

                    if ($update_beli) {
                        $res = [
                            'code' => 201,
                            'msg' => 'Po Pembelian Berhasil Diupdate',
                        ];
                    } else {
                        $res = [
                            'code' => 400,
                            'msg' => 'Gagal Update Po Pembelian !'
                        ];
                    }
                }
            }
        }
        
        return response()->json($res);
    }

    public function datatable_brg_detail(Request $req)
    {
        $id_beli_po = $req->_idBeliPo;
        $dt_beli = DB::table('beli_po')->where('id_beli', $id_beli_po)->first();

        $id = isset($dt_beli) ? $dt_beli->id : '';

        $data = DB::table('beli_detail_po as a')
                    ->where('a.status', NULL)
                    ->where('a.id_detail_beli', $id)
                    ->leftJoin('satuan as b', 'a.id_satuan', '=', 'b.id')
                    ->leftJoin('barang as c', 'a.id_brg', '=', 'c.kode')
                    ->select('a.id','a.nama_brg','a.ketr','a.id_satuan','a.qty','a.harga', 'a.subtotal',
                                'b.nama as satuan',
                                'c.kode')
                    ->get();
        
        return Datatables::of($data)
        ->addIndexColumn()
        ->addColumn('opsi', function ($data) {
            $id_item = $data->id;
            $nama_item = "'".$data->nama_brg."'";
            $harga_item = $data->harga;
            $kode_brg = $data->kode;

            $id_satuan = $data->id_satuan;
            $satuan = "'".$data->satuan."'";
            $qty = !is_null($data->qty) ? $data->qty : 0 ;
            $ketr = !is_null($data->ketr) ?  "'".$data->ketr."'" : "'"."'";
            $subtotal = ($qty * $harga_item);
            // return '<button type="button" class="btn btn-sm btn-primary" onclick="select_item('.$id_item.','.$kode_brg.','.$nama_item.','.$id_satuan.','.$satuan.','.$harga_item.','.$qty.','.$potongan.','.$ketr.','.$ketr_tambahan.','.$subtotal.','.$ketr_ganti_harga.','.$harga_new.')"><i class="fa fa-edit"></i></a>
            //         <button type="button" class="btn btn-sm btn-danger" onclick="delete_item('.$id_item.')"><i class="fa fa-trash"></i></button>';
                    return '<button type="button" class="btn btn-sm btn-danger" onclick="delete_item('.$id_item.')"><i class="fa fa-trash"></i></button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function delete_item(Request $req) 
    {
        $id_user = session::get('id_user');
        $id_item = $req->_idItem;

        $beli_detail_po = DB::table('beli_detail_po')->where('id', $id_item)->first();
        $id = $beli_detail_po->id_detail_beli;
       
        $res = [];

        $delete_item = DB::table('beli_detail_po')->where('id', $id_item)->delete();
        if($delete_item) {
            $jumlah = DB::table('beli_detail_po')->where('id_detail_beli',$id)->where('status',NULL)->sum('subtotal');
            $update_total = DB::table('beli_po')->where('id', $id)->update([
                'total' => $jumlah
            ]);
            // $this->hapus_jurnal_item($id_sj, $id_barang, $ketr_barang, $id);
            $res = [
                'code' => 300,
                'msg' => 'Data telah dihapus'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal dihapus'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }


    public function set_akun_dp($id_beli_po, $jenis_byr, $dp)
    {
        $beli_po = DB::table('beli_po as a')
                    ->leftJoin('suplier as b', 'a.suplier', '=', 'b.id')
                    ->where('a.id', $id_beli_po)
                    ->first();

        // kas/bank
        $akun[0]['tgl'] = date('Y-m-d');
        $akun[0]['id_item'] = NULL;
        $akun[0]['bm'] = $id_beli_po;
        $akun[0]['hpp'] = NULL;
        $akun[0]['grup'] = 5;
        $akun[0]['jenis_jurnal'] = 'beli-dp';
        $akun[0]['nama'] = $beli_po->nama; 
        $akun[0]['no_akun'] = '130';
        $akun[0]['keterangan'] = 'DP Pembelian No. PO '.$id_beli_po;
        $akun[0]['map'] = 'd';
        $akun[0]['hit'] = NULL;
        $akun[0]['qty'] = NULL;
        $akun[0]['harga'] = $dp;
        $akun[0]['total'] = $dp;
        
        // piutang
        $akun[1]['tgl'] = date('Y-m-d');
        $akun[1]['id_item'] = NULL;
        $akun[1]['bm'] =  $id_beli_po;
        $akun[1]['hpp'] = NULL;
        $akun[1]['grup'] = 5;
        $akun[1]['jenis_jurnal'] = 'beli-dp';
        $akun[1]['nama'] = $beli_po->nama;
        $akun[1]['no_akun'] = $jenis_byr == 'Transfer' ? 120 : 110;
        $akun[1]['keterangan'] = 'DP Pembelian No. PO '.$id_beli_po;
        $akun[1]['map'] = 'k';
        $akun[1]['hit'] = NULL;
        $akun[1]['qty'] = NULL;
        $akun[1]['harga'] = $dp;
        $akun[1]['total'] = $dp;

        $simpan_jurnal = DB::table('jurnal')->insert($akun);
    }

    public function add_dp(Request $req)
    {
        $user_add = session::get('id_user');
        $id_beli_po = $req->_idBeliPo;
        $dp = $req->_dp;
        $jenis_byr = $req->_jenisByr;
        $total_byr = str_replace(".","",$req->_total);
        $tgl = date('Y-m-d');

        $dt_dp = [
            // 'id_beli'           => $id_beli_po,
            'tgl'               => $tgl,
            'jenis_byr'         => $jenis_byr,
            'jenis_transaksi'   => 1,
            'total'             => $dp,
            'is_po'             => 1,
            'created_at'        => date('Y-m-d H:i:s'),
            'user_add'          => $user_add
        ];
        
        DB::beginTransaction();

        try {
            $dt_beli = DB::table('beli_po')->where('id_beli', $id_beli_po)->first();
            $dt_dp['id_beli'] = $dt_beli->id;
            if ($dp < $total_byr) {
                $insert = DB::table('transaksi_po_beli')->insert($dt_dp);
                $this->set_akun_dp($dt_beli->id, $jenis_byr, $dp);
                $res = [
                    'code'  => 200,
                    'msg'   => 'Data Berhasil Disimpan',
                    'dp'    => number_format($dp, 0, ',', '.')
                ];
            } else {
                $res = [
                    'code'  => 400,
                    'msg'   => 'Total Dp Melebihi total bayar',
                    'dp'    => null
                ];
            }
            
            DB::commit();
        } catch (Exception $th) {
            DB::rollback();
            $res = [
                'code'  => 400,
                'msg'   => 'Data Gagal Disimpan',
                'dp'    => null
            ];
        }

        $data['response'] = $res;
        return response()->json($data);
    }

    public function delete(Request $req)
    {
        $id = $req->_id;

        $delete = DB::table('beli_po')->where('id_beli', $id)->delete();

        if ($delete) {
            $delete_detail_po = DB::table('beli_detail_po')->where('id_detail_beli', $id)->delete();
            $delete_transaksi = DB::table('transaksi_po_beli')->where('id_beli', $id)->where('is_po', 1)->delete();
            $delete_jurnal = DB::table('jurnal')->where('jenis_jurnal', 'beli-dp')->where('ref', $id)->delete();
            $res = [
                'code'  => 200,
                'msg'   => 'Data Berhasil Dihapus'
            ];
        } else {
            $res = [
                'code'  => 400,
                'msg'   => 'Data Gagal Dihapus'
            ];
        }

        $data['response'] = $res;
        return response()->json($data);
    }

    public function batal_po(Request $req)
    {
        $id_users = session::get('id_user');
        $id = $req->_id;
        $id_beli = $req->_idBeli;
        $ket = $req->_ket;
        
        $data_btl_po = [
            'user_batal' => $id_users,
            'ket_batal' => $ket,
            'user_upd' => $id_users,
            'updated_at' => date("Y-m-d H:i:s")
        ];

        $cek_batal = DB::table('beli_po')->where('id', $id)->whereNotNull('user_batal')->first();

        if (isset($cek_batal)) {
            $res = [
                'code' => 400,
                'msg' => 'Data sudah pernah dibatalkan'
            ];
        } elseif (!isset($ket)){
            $res = [
                'code' => 400,
                'msg' => 'Isi Keterangan Pembatalan'
            ];
        } else {
            $update = DB::table('beli_po')->where('id', $id)->update($data_btl_po);

            if ($update) {

                $res = [
                    'code' => 200,
                    'msg' => 'Data berhasil dibatalkan'
                ];
            } else {
                $res = [
                    'code' => 400,
                    'msg' => 'Data gagal dibatalkan'
                ];
            }
        }
        return response()->json($res);
    }

    // Awal Pelunasan
    public function total(Request $req)
    {
        $id = $req->_id;
        $po = DB::table('beli_po')->where('id', $id)->first();
        $transaksi = DB::table('transaksi_po_beli')->where('id_beli', $id)->sum('total');
        // dd($transaksi);
        $data['total_byr'] = $po->total;
        $data['total'] = $transaksi;
        $data['sisa_tagihan'] = $po->total - $transaksi;
        // dd($data);
        return response()->json($data);
    }

    public function datatable_lunas(Request $req)
    {
        $id = $req->_id;
        $data = DB::table('transaksi_po_beli')->where('id_beli', $id)->get();

        return DataTables::of($data)
        ->addIndexColumn()
        ->editColumn('jenis_byr', function ($data) {
            $jenis_byr = $data->jenis_byr;
            $urut_byr = $data->jenis_transaksi;

            return 'pembayaran ke-'.$urut_byr.' ('.$jenis_byr.')'; 
        })
        ->make(true);
    }
    
    public function simpan_pelunasan(Request $req)
    {
        $id_user = session::get('id_user');
        $id = $req->_id;
        $id_beli = $req->_idBeli;
        $tgl = date('Y-m-d', strtotime($req->_tgl));
        $total_byr = $req->_totalByr;
        $jenis_byr = $req->_jenisByr;
        $sisa_tagihan = $req->_sisaTagihan;

        $po = DB::table('beli_po')->where('id', $id)->first();
        $po_detail = DB::table('beli_detail_po')->where('id_detail_beli', $id)->get();

        $no_byr = DB::table('transaksi_po_beli')
                            ->where('id_beli', $id)
                            ->where('is_po', 1)
                            ->max('jenis_transaksi');

        $dt_transaksi = [
            'id_beli'             => $id,
            'tgl'                 => $tgl,
            'total'               => $total_byr,
            'jenis_byr'           => $jenis_byr,
            'jenis_transaksi'     => $no_byr + 1,
            'is_po'               => 1
        ];

        $dt_beli = (array)$po;
        unset($dt_beli['id_beli']);
        unset($dt_beli['id']);
        unset($dt_beli['bayar']);

        $max_id_beli = DB::table('beli')->max('id_beli');
        $no_new = $max_id_beli + 1;
        $dt_beli['id_beli'] = $no_new;

        $dt_detail_beli = [];

        DB::beginTransaction();

        try {
            if ($total_byr > $sisa_tagihan) {
                $res = [
                    'code'  => 400,
                    'msg'   => 'Maaf Pembayaran Melebihi Total Tagihan'
                ];
            } else {
                $insert = DB::table('transaksi_po_beli')->insert($dt_transaksi);

                DB::commit();
                $transaksi = DB::table('transaksi_po_beli')
                                    ->where('id_beli', $id)
                                    ->where('is_po', 1)
                                    ->sum('total');

                // if ($po->total <= $transaksi) {
                    $dt_beli['bayar'] = $transaksi;
                    
                    // if (!isset($po->id_beli)) {
                        $update_id = DB::table('beli_po')->where('id', $id)->update(['id_beli' => $no_new]);
                        $insert_po = DB::table('beli')->insert($dt_beli);
                        foreach ($po_detail as $value) {
                            $dt_detail_beli[] = [
                                    // 'id'            => $value->id,
                                    'id_detail_beli'=> $no_new,
                                    'id_brg'        => $value->id_brg,
                                    'nama_brg'      => $value->nama_brg,
                                    'ketr'          => $value->ketr,
                                    'no_akun_brg'   => $value->no_akun_brg,
                                    'id_satuan'     => $value->id_satuan,
                                    'qty'           => $value->qty,
                                    'harga'         => $value->harga,
                                    'subtotal'      => $value->subtotal,
                                    'status'        => $value->status
                            ];
                        }
                        $insert_detail_po = DB::table('beli_detail')->insert($dt_detail_beli);
                        // $this->set_akun($dt_beli, $dt_detail_beli, $jenis_byr, $total_byr);
                    // }
                    $this->set_akun_dp($id, $jenis_byr, $total_byr);

                    $res = [
                        'code'  => 200,
                        'msg'   => 'Data Berhasil Disimpan'
                    ];
                // } elseif ($po->total > $transaksi) {
                //     $res = [
                //         'code'  => 400,
                //         'msg'   => 'Maaf Pembayaran Melebihi Total Tagihan'
                //     ];
                // }
            }
            
            
        } catch (Exception $th) {
            DB::rollback();
            $res = [
                'code'  => 400,
                'msg'   => 'Data Gagal Disimpan'.$th->getMessage()
            ];
        }

        $data['response'] = $res;
        return response()->json($data);
    }

    // Akhir Pelunasan

}
