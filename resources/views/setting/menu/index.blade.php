@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Main Menu</h2>
            <ul class="nav navbar-right panel_toolbox">
              <a href="{{ route('menu.form') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <div class="card-box table-responsive">
                  <table id="tbFitur" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Fitur</th>
                        <th>Icon</th>
                        <th>Link / Route</th>
                        <th>Parent</th>
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
    console.log('uyeee');

    var dataFitur = $('#tbFitur').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type: 'GET',
            url: '{{ route('menu.datatable') }}', 
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'nama', name: 'nama' },
          { data: 'icon', name: 'icon' },
          { data: 'route', name: 'route' },
          { data: 'parent', name: 'parent' },          
          { data: 'opsi', name: 'opsi' }
        ]
    });

    function delete_fitur(id) {
      $.ajax({
        type    : 'POST',
        url     : '{{ route('menu.delete') }}',
        data    : { '_id' : id },
        success : function (e) {
          notif(e.response.code, e.response.msg);
          dataFitur.draw();          
        }
      });
    }

  </script>
@endpush