<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
      <h3>General</h3>
      <ul class="nav side-menu">

      @php
        $get_id_akses = Session::get('akses');
        
        $akses = DB::table('akses_detail as a')
                ->where('a.id_akses', $get_id_akses)
                ->leftJoin('menu as b', 'a.id_menu', '=', 'b.id')
                ->select('b.id', 'b.nama', 'b.icon', 'b.route', 'b.parent_id')
                ->get();

        $dt = [];
        $no = 0;
        foreach ($akses as $key => $e) {
            if (!isset($e->parent_id)) {
                $dt[App\Http\Controllers\SampleController::detail_menu($e->id)->nama] = [
                                'nama' => App\Http\Controllers\SampleController::detail_menu($e->id)->nama, 
                                'icon' => App\Http\Controllers\SampleController::detail_menu($e->id)->icon,
                                'route' => App\Http\Controllers\SampleController::detail_menu($e->id)->route,
                                'child' => NULL 
                            ];        
            }else {
                $dt[App\Http\Controllers\SampleController::detail_menu($e->parent_id)->nama]['nama'] = App\Http\Controllers\SampleController::detail_menu($e->parent_id)->nama;
                $dt[App\Http\Controllers\SampleController::detail_menu($e->parent_id)->nama]['icon'] = App\Http\Controllers\SampleController::detail_menu($e->parent_id)->icon;
                $dt[App\Http\Controllers\SampleController::detail_menu($e->parent_id)->nama]['route'] = App\Http\Controllers\SampleController::detail_menu($e->parent_id)->route;
                $dt[App\Http\Controllers\SampleController::detail_menu($e->parent_id)->nama]['child'][] = $e;
            }
        }

        $bulan = array('Januari', 'Februari', 'Maret');
        $kendaraan = array('jenis' => 'Mobil', 'merk' => 'Toyota', 'tipe' => 'Vios');
        $fert = [];
        foreach ($bulan as $k) {
            $fert[] = $k;
        }

        $kd = [];
        foreach ($kendaraan as $key => $a) {
            $kd[] = $a;
        }
        $mmm = [];
        foreach ($dt as $key => $e) {
            $mmm[] = $e;
        }
        // $data['op'] = $dt;
        // dd($dt['menu']);
        // $data['menu'] = $dt;
        // $data['bulan'] = $bulan;
        // $data['ds'] = $kendaraan;
        // $data['dss'] = $kd;
        // $data['hs'] = $fert;
        // $data['hs_m'] = $mmm;
        // dd($mmm);
      @endphp
         
      @foreach ($mmm as $item)
        @if (isset($item['child']))
            <li><a><i class="{{ $item['icon']}}"></i> {{ $item['nama'] }} <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                  @foreach ($item['child'] as $key => $x)
                    <li><a href="{{ route($x->route) }}">{{ $x->nama }}</a></li>
                  @endforeach
                </ul>
            </li>
        @else 
            <li><a href="{{ route($item['route']) }}"><i class="{{ $item['icon']}}"></i> {{ $item['nama'] }} <span class="label label-success pull-right"></span></a></li>
        @endif
      @endforeach
    
    
      </li>
      </ul>
    </div>
</div>

