<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title></title>

    <!-- Bootstrap -->
    <link href="{{ URL::asset('/') }}ella/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ URL::asset('/') }}ellavendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ URL::asset('/') }}ellavendors/nprogress/nprogress.css" rel="stylesheet">
    
    <!-- Custom styling plus plugins -->
    <link href="{{ URL::asset('/') }}ella/build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="">
    <div class="container body">
      <div class="main_container">
        @yield('content')
      </div>
    </div>

    <!-- jQuery -->
    <script src="{{ URL::asset('/') }}ella/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
   <script src="{{ URL::asset('/') }}ella/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- FastClick -->
    <script src="{{ URL::asset('/') }}ella/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="{{ URL::asset('/') }}ella/vendors/nprogress/nprogress.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="{{ URL::asset('/') }}ella/build/js/custom.min.js"></script>
    @stack('js')
  </body>
</html>