@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
     
    <!-- Awal Modal Akun -->
     <div class="modal fade bs-example-modal-lg" id="modal_akun" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Data Akun</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <table class="table table-striped table-bordered" id="tb_akun" style="width:100%">
                      <thead>
                          <tr>
                              <th>No.</th>
                              <th>No. Akun</th>
                              <th>Nama</th>
                              <th>Opsi</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
    </div>
    <!-- Akhir Modal Akun -->

     <!-- Awal Modal Barang/Triplek -->
     <div class="modal fade bs-example-modal-lg" id="modal_brg" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Data Barang</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <table class="table table-striped table-bordered" id="tabel_brg" style="width:100%">
                      <thead>
                          <tr>
                              <th>No.</th>
                              <th>Nama</th>
                              <th>Harga</th>
                              <th>Satuan</th>
                              <th>Sisa</th>
                              <th>Opsi</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
    </div>
    <!-- Awal Modal barang/triplek -->

    <!-- Awal Modal DP -->
    <div class="modal fade bs-example-modal-lg" id="modal_dp" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Dp Pembelian</h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
           <form>
            <div class="item form-group">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Total Dp : <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 ">
                <input type="text" id="Dp" name="dp" required="required" class="form-control">
              </div>
            </div>
            <div class="item form-group">
              <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Jenis Bayar <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 ">
                  <select class="form-control" id="jenisPembayaran" name="pembayaran" autocomplete="off" required='required'>
                    <option></option>
                    <option>Tunai</option>
                    <option>Transfer</option>
                  </select>
              </div>
            </div>

            <div class="clearfix"></div>
            <div class="ln_solid"></div>
            
            <div class="form-group">
              <div class="offset-md-3">
                <button type="button" class="btn btn-sm btn-success" onclick="add_dp()">Submit</button>
              </div>
            </div>
          </form>
          </div>
        </div>
      </div>
    </div>
    <!-- Awal Modal DP -->

    <!-- ============================================================== -->
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>PO Pembelian</h2>
              <ul class="nav navbar-right panel_toolbox">
                <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#modal_dp">DP</button>
                <a href=" {{ route('pobeli.form') }} " class="btn btn-sm btn-success">New</a>
                <a href=" {{  route('pobeli.index') }} " class="btn btn-sm btn-secondary"><i class="fa fa-arrow-left"></i> Back </a>
              </ul>
              <div class="clearfix"></div>
            </div>

            <div class="x_content">
              <br/>
              <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
                {{-- {{ csrf_field() }} --}}
                {{-- Kiri --}}
                <div class="col-md-6 col-sm-6">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">ID Beli PO <span class="required">*</span>
                    </label>
                    <div class="col-md-2 col-sm-2 ">
                      <input type="text" id="Form" name="form" required="required" class="form-control" readonly value="{{ !empty($form) ? $form : '' }}">
                      <input type="text" id="idBeliPo" name="id_beli_po" required="required" class="form-control" readonly value="{{ !empty($id_beli) ? $id_beli : '' }}">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Tanggal
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Tgl" name="tgl" required='required' class="form-control" value="{{ !empty($tgl) ? $tgl : '' }}">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Dari <span class="required">*</span>
                    </label>
                    <div class="col-md-6">
                      <select class="form-control" name="id_suplier" id="idSuplier" autocomplete="off" required='required'>
                        <option value="" id="suplier_kosong" >-</option>
                        @php
                            $id_spl = !empty($id_suplier) ? $id_suplier : '';
                            $pmb = !empty($pembayaran) ? $pembayaran : '';
                            $png = !empty($pengiriman) ? $pengiriman : '';
                            $knd = !empty($id_kendaraan) ? $id_kendaraan : ''; 
                            $kry = !empty($id_karyawan) ? $id_karyawan : '';
                            $no_rek = !empty($no_rek) ? $no_rek : '';
                            $opsi =  !empty($opsi) ? $opsi : '';
                        @endphp
                        @foreach ($suplier as $item)
                          <option value="{{ $item->id }}" @if($item->id == $id_spl) selected @endif>{{ $item->nama }}</option>                              
                        @endforeach
                      </select>
                    </div>
                    {{-- <div class="col-md-1">
                      <button type="button" class="btn btn-info pelangganQ"><i class="fa fa-plus"></i></button>
                    </div> --}}
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Opsi <span class="required">*</span>
                    </label>
                    <div class="col-md-6">
                      <select class="form-control" name="opsi" id="Opsi" autocomplete="off" required='required'>
                        <option value="" id="opsiKosong" >-</option>  
                        <option value="1" @if ($opsi == 1) selected @endif >Triplek</option>                              
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-md-6 col-sm-6">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Nota <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="noNotaSpl" name="no_nota_spl" required="required" class="form-control" value="{{ !empty($no_nota_spl) ? $no_nota_spl : ''}}" autocomplete="off">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No Akun Kredit <span class="required">*</span>
                    </label>
                    <div class="col-md-6">
                      <input type="text" id="noAkunKredit" name="no_akun_kredit" required='required' class="form-control noAkunQ" value="{{ !empty($no_akun) ? $no_akun : '' }}" autocomplete="off">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama Akun
                    </label>
                    <div class="col-md-6 ">
                      <input type="text" id="namaAkunKredit" name="nama_akun_kredit" class="form-control" readonly value="{{ !empty($nama_akun) ? $nama_akun : '' }}">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Total
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Total" name="total" class="form-control" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Dp
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="dpQ" name="dp_q" class="form-control" value="{{ !empty($dp) ? $dp : '' }}" readonly>
                    </div>
                  </div>
                </div>

                <div class="clearfix"></div>
                <div class="ln_solid"></div>
                {{-- kiri --}}
                <div class="col-md-6 col-sm-6">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama Barang
                    </label>
                    <div class="col-md-5 ">
                      <input type="text" id="idItem" name="id_item" class="form-control" autocomplete="off" readonly>
                      <input type="text" id="idBrg" name="id_brg" class="form-control" autocomplete="off" readonly>
                      <input type="text" id="namaBrg" name="nama_brg" class="form-control brgQ" autocomplete="off">
                    </div>
                    <div class="col-md-1 ">
                      <button type="button" class="btn btn-sm btn-info barangQ"><i class="fa fa-plus"></i></button>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Akun
                    </label>
                    <div class="col-md-6 ">
                      <input type="text" id="noAkunDebit" name="no_akun_debit" class="form-control" autocomplete="off" readonly>
                    </div>
                    {{-- <div class="col-md-1 ">
                      <button type="button" class="btn btn-sm btn-info" onclick="new_barang()">New</button>
                    </div> --}}
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Satuan
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <select id="idSatuan"  name="id_satuan" class="form-control" autocomplete="off">
                        <option value="">-</option>
                        @foreach ($satuan as $item)
                          <option value="{{ $item->id }}">{{ $item->nama }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>  

                {{-- kanan --}}
                <div class="col-md-6 col-sm-6">
                  {{-- <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">m3
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="M3" name="m3" class="form-control">
                    </div>
                  </div> --}}
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Qty
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Qty" name="qty" class="form-control qtyQ" autocomplete="off">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Harga Jual
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Harga" name="harga" class="form-control" autocomplete="off" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Hpp
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Hpp" name="hpp" class="form-control" autocomplete="off" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Subtotal
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Subtotal" name="subtotal" class="form-control" autocomplete="off" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Ketr
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Ketr" name="ketr" class="form-control" autocomplete="off">
                    </div>
                  </div>
                </div>

                <div class="clearfix"></div>
                <div class="ln_solid"></div>
                <div class="col-md-6 col-sm-6">
                  <div class="form-group">
                    <div class="offset-md-3">
                      {{-- <button type="button" class="btn btn-primary">Cancel</button> --}}
                      <button class="btn btn-sm btn-primary" type="reset">Reset</button>
                      <button type="button" class="btn btn-sm btn-success" onclick="simpan_sj()">Submit</button>
                      {{-- <button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#modal_checkout">Checkout</button> --}}
                      {{-- <button type="button" class="btn btn-info" onclick="print_sj()">Print</button> --}}
                    </div>
                  </div>
                </div>
              </div>                                   
              </form>

              <div class="clearfix"></div>
                <div class="ln_solid"></div>
                <table class="table table-striped table-bordered" id="tbItem" style="width:100%">
                  <thead>
                      <tr>
                          <th>No.</th>
                          <th>Nama</th>
                          <th>Ketr</th>
                          <th>Satuan</th>
                          <th>Qty</th>
                          <th>Harga</th>
                          <th>Subtotal</th>
                          <th>Opsi</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
              </table>

              <div class="clearfix"></div>
              <div class="ln_solid"></div>
              </div>
          </div>
        </div>
      </div>
  </div>
</div>        
@endsection

@push('js')
  <script type="text/javascript">
  $('#Tgl').datetimepicker({
      format: 'DD-MM-YYYY'
    });
  var form = $('#Form').val();
  
  if (form == 'edit') {
  } else {
    no_urut();
  }

  hitung_total();
  
  function hitung_subtotal(hpp, qty) {
    var subtotal = ( hpp * qty );
    $('input[name=subtotal]').val(subtotal);
  }

    //subtotal
    $(document).on('keyup', '.qtyQ', function(){
      var qty = $('input[name=qty]').val();
      var hpp =  $('input[name=hpp]').val();

      hitung_subtotal(hpp, qty);
    });

    $(document).on('keyup', '.ketrQ', function(){ 
      var qty = $('input[name=qty]').val();
      var hpp =  $('input[name=hpp]').val();

      hitung_subtotal(hpp, qty);
    });

    var tb_barang = $('#tabel_brg').DataTable({
      processing: true,
      serverSide: true,
      ajax: {
          type: 'GET',
          url: '{{ route('pobeli.datatable_brg') }}', 
      },
      columns: [
        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
        { data: 'nama_brg', name: 'nama_brg' },
        { data: 'harga', name: 'harga' },
        { data: 'satuan', name: 'satuan' },
        { data: 'sisa', name: 'sisa' },
        { data: 'opsi', name: 'opsi' },
      ]
    });

    var tb_barang_detail = $('#tbItem').DataTable({
      processing: true,
      serverSide: true,
      ajax: {
          type : 'POST',
          url : '{{ route('pobeli.datatable_brg_detail') }}',
          data : function (e) {
            e._idBeliPo = $('#idBeliPo').val();
          }
      },
      columns : [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'nama_brg', name:'nama_brg' },
          { data: 'ketr', name:'ketr' },
          { data: 'satuan', name:'satuan' },
          { data: 'qty', name:'qty' },
          { data: 'harga', name:'harga' },
          { data: 'subtotal', name:'subtotal' },
          { data: 'opsi', name:'opsi' }
      ]
    });

    function no_urut() {
      $.ajax({
              type : 'GET',
              url : '{{ route('pobeli.no_urut_pobeli') }}',
              success : function (e) {
                console.log(e);
                $('#idBeliPo').val(e);
              }
      });
    }

    function hitung_total() {
      var id = $('input[name=id_beli_po]').val();

      $.ajax({
        type: 'POST',
        url: '{{ route('pobeli.hitung_total') }}',
        data: { '_id' : id },
        success : function (e) {
         console.log(e);
         
          $('#Total').val(e.total);
          
        }
      })
    }

    function simpan_sj() {
      var id_beli_po = $('#idBeliPo').val();
      var no_nota_spl = $('#noNotaSpl').val();
      var tgl = $('#Tgl').val();
      var id_suplier = $('#idSuplier').val();
      var no_akun_k = $('#noAkunKredit').val();
      var opsi = $('#Opsi').val();
      var dp = $('#Dp').val();

      var id_item = $('#idItem').val();
      var id_brg = $('#idBrg').val();
      var nama_brg = $('#namaBrg').val();
      var stok = $('#Stok').val();
      var hpp = $('#Hpp').val();
      var harga = $('#Harga').val();
      var no_akun_brg = $('#noAkunDebit').val();
      var id_satuan = $('#idSatuan').val();
      var subtotal = $('#Subtotal').val();
      var qty = $('#Qty').val();
      var potongan = $('#Potongan').val();
      var ketr = $('#Ketr').val();

        $.ajax({
          type : 'POST',
          url : '{{ route('pobeli.save') }}',
          data : {
                  '_idBeliPo' : id_beli_po,
                  '_tgl' : tgl,
                  '_idSuplier' : id_suplier,
                  '_noNotaSpl' : no_nota_spl,
                  '_opsi' : opsi,
                  '_noAkunK' : no_akun_k,
                  '_dp' : dp,
                  '_idItem' : id_item,
                  '_idBrg' : id_brg,
                  '_namaBrg' : nama_brg,
                  '_noAkunD' : no_akun_brg,
                  '_stok' : stok,
                  '_hpp' : hpp,
                  '_harga' : harga,
                  '_idSatuan' : id_satuan,
                  '_subtotal' : subtotal,
                  '_qty' : qty,
                  '_potongan' : potongan,
                  '_ketr' : ketr,
                },
          success: function (e) {
            clear_barang();
            notif(e.code, e.msg);
            tb_barang_detail.draw();
            tb_barang.draw();
            $('input[name=idItem]').val('');
            hitung_total();
          }
        });
    }

    $(document).on('click', '.brgQ', function(){  
      $('#modal_brg').modal('show');  
    });

    function select_brg(kode_brg,nama_brg,hpp,harga, id_satuan, satuan, sisa, no_akun) {
      $('#modal_brg').modal('hide');
      $('input[name=id_brg]').val(kode_brg);
      $('input[name=nama_brg]').val(nama_brg);
      $('input[name=hpp]').val(hpp);
      $('input[name=harga]').val(harga);
      $('input[name=id_satuan]').val(id_satuan);
      $('input[name=satuan]').val(satuan);
      $('input[name=stok]').val(sisa);
      $('input[name=no_akun_debit]').val(no_akun)
      clear_barang2();
    }   

    function clear_barang() {
      $('input[name=no]').val('');
      $('input[name=id_brg]').val('');
      $('input[name=nama_brg]').val('');
      $('input[name=hpp]').val('');
      $('input[name=harga]').val('');
      $('input[name=id_satuan]').val('');
      $('input[name=satuan]').val('');
      $('input[name=stok]').val('');
      $('input[name=no_akun_debit]').val('');
      clear_barang2();
    }

    function new_barang() {
      $('input[name=id_brg]').val('');
      $('input[name=idItem]').val('');
      clear_barang();
    }
    
    function clear_barang2() {
      $('input[name=qty]').val('');
      $('input[name=ketr]').val('');
      $('input[name=subtotal]').val('');
    }

    function delete_item(id) {
      $.ajax({
        type : 'POST',
        url : '{{ route('pobeli.delete_item') }}',
        data : { '_idItem' : id },
        success : function (e) {
          notif(e.response.code, e.response.msg);
          tb_barang_detail.draw();
          tb_barang.draw();
          hitung_total();
        }
      });
    }

    $(document).on('click', '.noAkunQ', function(){  
      $('#modal_akun').modal('show');  
    });

    var tb_akun = '';

    tb_akun = $('#tb_akun').DataTable({
      processing    : true,
      serverSide    : true,
      ajax  : {
            type: 'GET',
            url : '{{ route('pembelian.datatable_akun') }}', 
      },
      columns:[
        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
        { data: 'no_akun', data: 'no_akun' },
        { data: 'akun', data: 'akun'},
        { data: 'opsi', data: 'opsi'}
      ]
    });

    $('#Opsi').change(function () {
      var id_p = $(this).val();

      if (id_p == 1) {
        $('#noAkunKredit').val(210)
        $('#noAkunKredit').attr('disabled', true)
      } else {
        $('#noAkunKredit').val('')
        $('#noAkunKredit').attr('disabled', false)
      // console.log(id_p);
      }
    });      

    function add_dp() {
      var jenis_byr = $('#jenisPembayaran').val();
      var dp = $('#Dp').val();
      var id_beli_po = $('#idBeliPo').val();
      var total_byr = $('#Total').val();

      $.ajax({
          type : 'POST',
          url : '{{ route('pobeli.add_dp') }}',
          data : {
                  '_jenisByr' : jenis_byr,
                  '_dp' : dp,
                  '_idBeliPo' : id_beli_po,
                  '_total' : total_byr
                },
          success: function (e) {
            notif(e.response.code, e.response.msg);
            $('#modal_dp').modal('hide');
            $('#dpQ').val(e.response.dp);
          }
        });

    }
    
    function select_akun(no_akun, akun) {
      $('#modal_akun').modal('hide');
      $('#noAkunKredit').val(no_akun);
      $('#namaAkunKredit').val(akun);
    }

  </script>
@endpush