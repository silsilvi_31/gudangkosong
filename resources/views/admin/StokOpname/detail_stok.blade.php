@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

    {{-- Modal Cek Nota --}}
    <div id="modal_ceknota" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Cek Nota</h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
            {{-- Kiri --}}
            <div class="col-md-6 col-sm-6">
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Nota <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="hidden" id="formSj" name="" class="form-control" readonly>
                  <input type="text" id="" name="id" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Tanggal
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="tgl" name="tgl" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Kepada
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="id_pelanggan" name="id_pelanggan" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Catatan Khusus
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="catatan" class="form-control" readonly>
                </div>
              </div>
            </div>

            {{-- Kanan --}}
            <div class="col-md-6 col-sm-6">
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Pembayaran
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="pembayaran" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Pengiriman
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="pengiriman" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Mobil
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="id_kendaraan" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Sopir
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="" name="id_karyawan" class="form-control" readonly>
                </div>
              </div>
            </div>
            </form>
            <div class="clearfix"></div>
            <div class="ln_solid"></div>
            <table class="table table-striped table-bordered" id="tb_ceknota" style="width:100%">
              <thead>
                  <tr>
                      <th>No.</th>
                      <th>Nama</th>
                      <th>Ketr</th>
                      <th>Satuan</th>
                      <th>Qty</th>
                      <th>Harga</th>
                      <th>Potongan</th>
                      <th>Subtotal</th>
                      <th>Opsi</th>
                  </tr>
              </thead>
              <tbody>
              </tbody>
          </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Approve</button>
          </div>

        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>{{ $nama_brg }}</h2>
            <ul class="nav navbar-right panel_toolbox">
              {{-- <a href="{{ route('sjdefault.form') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a> --}}
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <input type="hidden" id="kdBrg" value="{{ $kode }}">
                <div class="card-box table-responsive">
                  <table id="tb_item" class="table table-sm table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No. Nota</th>
                        <th>Tanggal</th>
                        <th>Stok Awal</th>
                        <th>Stok Masuk</th>
                        <th>Stok Keluar</th>
                        <th>Sisa</th>
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
      $('#tglExport_m').datetimepicker({
        format: 'YYYY-MM-DD'
      });

      $('#tglExport_a').datetimepicker({
        format: 'YYYY-MM-DD'
      });
    // console.log('uyeee');
    var datatableSj = $('#tb_item').DataTable({
        processing: true,
        serverSide: true,
        order: false,
        ajax: {
            type: 'POST',
            url: '{{ route('stok.datatable_detail') }}', 
            data: function (e) {
                  e._kodeBrg = $('#kdBrg').val();
            }
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'tgl', name: 'tgl' },
          { data: 'stok_awal', name: 'stok_awal' },
          { data: 'stok_masuk', name: 'stok_masuk' },
          { data: 'stok_keluar', name: 'stok_keluar' },
          { data: 'sisa', name: 'sisa' },
          { data: 'opsi', name: 'opsi' }
        ]
    });

  </script>
@endpush