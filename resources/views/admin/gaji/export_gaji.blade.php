@php
    // dd($rekap);

    $data_gaji = [];
    $tt = 0;
    $tt_lembur = 0;
    $tt_potongan = 0;
    $total_seminggu = 0;
    $tt_jumat = 0;
    $tt_sabtu = 0;
    $tt_minggu = 0;
    $tt_senin = 0;
    $tt_selasa = 0;
    $tt_rabu = 0;
    $tt_kamis = 0;

    foreach ($data['rekap'] as $va) {
      $gj_jumat = !empty($va->jumat) ? $va->jumat->total_gaji : null;
      $gj_sabtu = !empty($va->sabtu) ? $va->sabtu->total_gaji : null;
      $gj_minggu = !empty($va->minggu) ? $va->minggu->total_gaji : null;
      $gj_senin = !empty($va->senin) ? $va->senin->total_gaji : null;
      $gj_selasa = !empty($va->selasa) ? $va->selasa->total_gaji : null;
      $gj_rabu = !empty($va->rabu) ? $va->rabu->total_gaji : null;
      $gj_kamis = !empty($va->kamis) ? $va->kamis->total_gaji : null;

      $lembur_jumat = !empty($va->jumat) ? $va->jumat->lembur : null;
      $lembur_sabtu = !empty($va->sabtu) ? $va->sabtu->lembur : null;
      $lembur_minggu = !empty($va->minggu) ? $va->minggu->lembur : null;
      $lembur_senin = !empty($va->senin) ? $va->senin->lembur : null;
      $lembur_selasa = !empty($va->selasa) ? $va->selasa->lembur : null;
      $lembur_rabu = !empty($va->rabu) ? $va->rabu->lembur : null;
      $lembur_kamis = !empty($va->kamis) ? $va->kamis->lembur : null;

      $pot_jumat = !empty($va->jumat) ? $va->jumat->potongan : null;
      $pot_sabtu = !empty($va->sabtu) ? $va->sabtu->potongan : null;
      $pot_minggu = !empty($va->minggu) ? $va->minggu->potongan : null;
      $pot_senin = !empty($va->senin) ? $va->senin->potongan : null;
      $pot_selasa = !empty($va->selasa) ? $va->selasa->potongan : null;
      $pot_rabu = !empty($va->rabu) ? $va->rabu->potongan : null;
      $pot_kamis = !empty($va->kamis) ? $va->kamis->potongan : null;

      $tt_seminggu = $gj_jumat + $gj_sabtu + $gj_minggu + $gj_senin + $gj_selasa + $gj_rabu + $gj_kamis;
      $lembur_seminggu = $lembur_jumat + $lembur_sabtu + $lembur_minggu + $lembur_senin + $lembur_selasa + $lembur_rabu + $lembur_kamis;
      $pot_seminggu = $pot_jumat + $pot_sabtu + $pot_minggu + $pot_senin + $pot_selasa + $pot_rabu + $pot_kamis;
      $total_gaji_seminggu = $tt_seminggu + $lembur_seminggu - $pot_seminggu;

      $data_gaji[] = (object) [
                        'kodep' => $va->kodep,
                        'nama' => $va->nama,
                        'gj_jumat' => $gj_jumat,
                        'gj_sabtu' => $gj_sabtu,
                        'gj_minggu' => $gj_minggu,
                        'gj_senin' =>  $gj_senin,
                        'gj_selasa' => $gj_selasa,
                        'gj_rabu' => $gj_rabu,
                        'gj_kamis' => $gj_kamis,

                        'lembur_jumat' => $lembur_jumat,
                        'lembur_sabtu' => $lembur_sabtu,
                        'lembur_minggu' => $lembur_minggu,
                        'lembur_senin' => $lembur_senin,
                        'lembur_selasa' => $lembur_selasa,
                        'lembur_rabu' => $lembur_rabu,
                        'lembur_kamis' => $lembur_kamis,

                        'pot_jumat' => $pot_jumat,
                        'pot_sabtu' => $pot_sabtu,
                        'pot_minggu' => $pot_minggu,
                        'pot_senin' => $pot_senin,
                        'pot_selasa' => $pot_selasa,
                        'pot_rabu' => $pot_rabu,
                        'pot_kamis' => $pot_kamis,

                        'tt_seminggu' => $tt_seminggu,
                        'lembur_seminggu' => $lembur_seminggu,
                        'pot_seminggu' => $pot_seminggu,
                        'total_gaji_seminggu' => $total_gaji_seminggu
                      ];
      
      $tt += $tt_seminggu;
      $tt_lembur += $lembur_seminggu;
      $tt_potongan += $pot_seminggu;
      $total_seminggu += $total_gaji_seminggu;
      $tt_jumat += $gj_jumat;
      $tt_sabtu += $gj_sabtu;
      $tt_minggu += $gj_minggu;
      $tt_senin += $gj_senin;
      $tt_selasa += $gj_selasa;
      $tt_rabu += $gj_rabu;
      $tt_kamis += $gj_kamis;

    }
    // dd($tt);
@endphp

<table>
  <tr>
    <th></th>
    <th></th>
    <th>{{ $tt_jumat }}</th>
    <th>{{ $tt_sabtu }}</th>
    <th>{{ $tt_minggu }}</th>
    <th>{{ $tt_senin }}</th>
    <th>{{ $tt_selasa }}</th>
    <th>{{ $tt_rabu }}</th>
    <th>{{ $tt_kamis }}</th>
    <th>{{ $tt }}</th>
    <th>{{ $tt_lembur }}</th>
    <th>{{ $tt_potongan }}</th>
    <th>{{ $total_seminggu }}</th>
  </tr>
  <tr>
      <th>Kodep</th>
      <th>Nama</th>
      <th>Jumat</th>
      <th>Sabtu</th>
      <th>Minggu</th>
      <th>Senin</th>
      <th>Selasa</th>
      <th>Rabu</th>
      <th>Kamis</th>
      <th>tt</th>
      <th>Lembur</th>
      <th>Potongan</th>
      <th>Total</th>
  </tr>
  @foreach ($data_gaji as $item)
      <tr>
          <td>{{ $item->kodep }}</td>
          <td>{{ $item->nama }}</td>
          <td>{{ $item->gj_jumat }}</td>
          <td>{{ $item->gj_sabtu }}</td>
          <td>{{ $item->gj_minggu }}</td>
          <td>{{ $item->gj_senin }}</td>
          <td>{{ $item->gj_selasa }}</td>
          <td>{{ $item->gj_rabu }}</td>
          <td>{{ $item->gj_kamis }}</td>
          <td>{{ $item->tt_seminggu }}</td>
          <td>{{ $item->lembur_seminggu }}</td>
          <td>{{ $item->pot_seminggu }}</td>
          <td>{{ $item->total_gaji_seminggu }}</td>
      </tr>
  @endforeach
</table>