@extends('master.print')
    
@section('content')
<style type="text/css">
  body {
    /* border: 1px solid #000 !important; */
    font-size : 1.3em !important;
  }
  .table-bordered, .table th {
    border: 1px solid #000 !important;
    padding: .3rem;
  }
  .x_panel {
    border : none !important;
  }
  .table td {
    border: none !important;
    padding: .3rem;
  }
  .nominal {
    float: right;
  }
  tr .total_gf {
    border: 1px solid #000 !important;
  }

</style>
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

        <!-- page content -->
            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">

                    <section class="content invoice">
                      <center><h1>KWITANSI</h1></center>
                       <!-- title row -->

                      <div class="row">
                        <div class="col-md-3">
                         <strong>No. Kwi</strong> 
                        </div>
                        <div class="col-md-6">
                          : {{ $no_kwi }}
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-3">
                          <strong>Tanggal</strong> 
                          </div>
                          <div class="col-md-6">
                            : {{ date('d M yy', strtotime($tgl)) }}
                          </div>
                      </div>

                      <div class="row">
                        <div class="col-md-3">
                          <strong>Telah Diterima dari </strong> 
                          </div>
                          <div class="col-md-6">
                            : {{ $dari }}
                          </div>
                      </div>

                      <div class="row">
                        <div class="col-md-3">
                          <strong>Uang sebesar </strong> 
                          </div>
                          <div class="col-md-6">
                            : Rp. {{ number_format($total, 0,',','.') }}
                          </div>
                      </div>

                      <div class="row">
                        <div class="col-md-3">
                          <strong>Untuk Pembayaran </strong> 
                          </div>
                          <div class="col-md-6">
                            : {{ $ketr }}
                          </div>
                      </div>

                      <div class="clearfix"></div>
                      <div class="ln_solid"></div>

                      <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-md-3">
                          <p class="lead"> </p>
                          <br>
                          <br>
                          {{-- <p></strong>{{ $cek_nota }}</strong></p> --}}
                        </div>

                        <div class="col-md-3">
                          <p class="lead"> </p>
                          <br>
                          <br>
                          <p></strong></strong></p>
                          </div>

                        <div class="col-md-3">
                          <center><p class="lead">Yang Menyerahkan,</p></center> 
                          <br>
                          <br>
                          <p> <center>{{ $pembuat }}</center> 
                        </div>

                        <div class="col-md-3">
                          <center><p class="lead">Yang Menerima,</p></center> 
                          <br>
                          <br>
                          <p> <center>{{ $penerima }}</center> 
                        </div>
                        <!-- /.col -->
                        
                        </div>
                       
                    </section>
                  </div>
                </div>
              </div>
            </div>
        <!-- /page content -->

@endsection

@push('js')
  <script type="text/javascript">
    $(function(){
      // window.print();
    }); 
  </script>
@endpush