@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

      {{-- Modal Cek Nota --}}
    <div id="modal_tutupbuku" class="modal fade bs-example-modal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Tutup Buku</h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
            {{-- Kiri --}}
            <div class="col-md-12 col-sm-12">
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Dari Tgl. <span class="required">*</span>
                </label>
                <div class="col-md-7 ">
                  <input type="text" id="dariTgl" name="dari_tgl" required="required" class="form-control " autocomplete="off" value="{{ $tgl_awalan }}">
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Sampai Tgl.
                </label>
                <div class="col-md-7">
                  <input type="text" id="sampaiTgl" name="sampai_tgl" required="required" class="form-control" autocomplete="off">
                </div>
              </div>
              <div class="alert alert-warning alert-dismissible " role="alert">
                <span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span>
                </button>
                Setelah tutup buku, Anda tidak dapat melakukan perubahan terhadap buku Anda pada tanggal SEBELUM <strong><span id="tglQ"></span></strong>
              </div>
            </div>
            </form>
          </div>
          <div class="modal-footer">
            <div class="offset-md-3">
              <button type="button" class="btn btn-success" id="btn-setuju" onclick="tutup_buku()" >Submit</button>
            </div>
          </div>
        </div>
      </div>
    </div>


      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Buka & Tutup Buku</h2>
            <ul class="nav navbar-right panel_toolbox">
              <a href="{{ route('bukaBuku.index') }}" class="btn btn-sm btn-success">Buka Buku</a>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <div class="card-box">
                  <div class="card-box table-responsive">
                    <table id="tb_parent_jurnal" class="table table-striped table-bordered" style="width:100%">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Tgl Awal</th>
                          <th>Tgl Akhir</th>
                          <th>Labarugi</th>
                          <th>Status</th>
                          <th>Opsi</th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
     $('#dariTgl').datetimepicker({
        format: 'DD-MM-YYYY'
    });
   
    $('#sampaiTgl').datetimepicker({
        format: 'DD-MM-YYYY'
    });

    $("#sampaiTgl").on("dp.change", function (e) {
        var tgl = $(this).val();

        if (!tgl) {
          $('#tglQ').text('');
        } else {
          $('#tglQ').text(tgl);
        }
    });

    

    // function list_tutup_buku() {
    //   var dari_tgl = $('#dariTgl').val();
    //   var sampai_tgl = $('#sampaiTgl').val();

    //   var tgl_gabung = tgl_m+'&'+tgl_a;

    //   var url = '{{ route('sjdefault.excel_sj', ['.p']) }}';
    //   var url_fix = url.replaceAll('.p',tgl_gabung);
    //   // console.log(url_fix);
    //   window.location.href = url_fix;
    // }

    function tutup_buku() {
      var dari_tgl = $('#dariTgl').val();
      var sampai_tgl = $('#sampaiTgl').val();

      var txt;
      var r =  confirm('Apakah Anda yakin Melakuan tutup buku pada '+dari_tgl+' sampai '+sampai_tgl+'?');

      if (r == true) {
        $.ajax({
        type  : 'POST',
        url : '{{ route('tutupBuku.simpan_tutupbuku') }}',
        data : {
              '_tgl' : dari_tgl,
              '_tglDua' : sampai_tgl,
        },
        success: function (e) {
          console.log(e);
          $('#modal_tutupbuku').modal('hide');
          notif(e.response.code, e.response.msg);
          datatable_parent_jurnal.draw();
        }
      });
      } else {
        txt = 'n';
      }

      
    }

    var datatable_parent_jurnal = '';

    var datatable_parent_jurnal = $('#tb_parent_jurnal').DataTable({
      processing: true,
      serverSide: true,
      order : false,
      ajax : {
        type : 'POST',
        url : '{{ route('tutupBuku.datatable_tutupbuku') }}',
        data : function (e) {
          e.tgl = $('#tgl').val();
          e.jenisJurnal = $('#jenisJurnal').val();
        }
      },
      columns: [
          { data: 'id', name: 'id' },
          { data: 'tgl_awal', name: 'tgl_awal' },
          { data: 'tgl_akhir', name: 'tgl_akhir' },
          { data: 'labarugi', name: 'labarugi' },
          { data: 'status', name: 'status' },
          { data: 'opsi', name: 'opsi' }
        ],
    });

    



  </script>
@endpush