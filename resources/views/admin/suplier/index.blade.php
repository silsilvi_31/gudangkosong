@extends('master.monster')
@section('title', 'Resep')
{{-- @section('menu1', 'active') --}}

@section('content')
<div class="container-fluid">
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Suplier</li>
            </ol>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-block">
                    <div class="pull-right">
                        <a href="{{ route('suplier.form') }} " class="btn btn-primary"><i class="fa fa-plus"></i> New</a>
                    </div>
                    <h4 class="card-title">Suplier</h4>
                    <div class="table-responsive m-t-40">
                        <table id="tb_suplier" class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>No. </th>
                                    <th>Nama Suplier</th>
                                    <th>Alamat</th>
                                    <th>No. Telp</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
    <script type="text/javascript">  
        console.log('supplier');
        var tb_suplier = $('#tb_suplier').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                type: 'GET',
                url: '{{ route('suplier.datatable') }}', 
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'nama', name: 'nama' },
                { data: 'alamat', name: 'alamat' },
                { data: 'no_telp', name: 'no_telp' },
                { data: 'opsi', name: 'opsi' }
            ]
        });
    </script>
@endpush