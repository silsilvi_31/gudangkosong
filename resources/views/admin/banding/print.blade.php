<!DOCTYPE html>
<html>
<head>
<style type="text/css">
@page{ 

margin-top:0px;

}
  .row {
    font-size : 1.1em;
  }

  .table_body, .th_b, .td_b{
    margin-left: 6%;
    border: 1px solid #000 !important;
    padding: .3rem;  
  }

table, td, th {
    /* border-collapse: collapse; */
    margin-left: 6%;
    font-size : 1.1em;
}

table {
    width: 94%;
}

/* th {
    height: 30px;
} */
</style>
</head>
<body>
@extends('master.print')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

        <!-- page content -->
            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                    <table>
                        <tr>
                            <div class="row_h">
                                <div class="col-md-11" align="center">
                                    <h1>BARANG KELUAR</h1>
                                </div>
                                <div class="col-md-1" align="center">
                                    <h1>{{$jenis_bk}}</h1>
                                </div>
                            </div>
                            <table>
                                <tr>
                                    <td style="width:50px">No</td>
                                    <td>: {{ $no_bk }}</td>
                                </tr>
                                <tr>
                                    <td style="width:50px">Tanggal</td>
                                    <td>: {{ date('d M Y', strtotime($tanggal)) }}</td>
                                </tr>
                                <tr>
                                    <td style="width:50px">Kepada</td>
                                    <td>: {{ $kepada }}</td>
                                </tr>
                            </table>
                        </tr>
                        <br>
                        <tr>
                            <table class="table_body">
                                <tr>
                                    @if ($jenis_bk == 'U')
                                    <th class="th_b">No. </th>
                                    <th class="th_b">Nama Barang</th>
                                    <th class="th_b">Banyak</th>
                                    <th class="th_b">Harga</th>
                                    <th class="th_b">Sub Total</th>
                                    @foreach ($bk_detail as $item)
                                    @if(isset( $item->keterangan))
                                    <th class="th_b">Keterangan</th>
                                    @else
                                    @endif
                                    @endforeach
                                </tr>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach ($bk_detail as $item)
                                <tr>
                                    <td class="td_b">{{ $no++ }}</td>
                                    <td class="td_b">{{ $item->nama_brg }}</td>
                                    <td class="td_b">{{ $item->qty }} </td>
                                    <td class="td_b">{{ number_format($item->harga, 0, ',', '.') }} </td>
                                    <td class="td_b">{{ number_format($item->sub_total, 0, ',', '.') }} </td>
                                    @if(isset($item->keterangan))
                                    <td class="td_b">{{ $item->keterangan }} </td>
                                    @else
                                    @endif      
                                </tr>   
                                @endforeach
                                <tr>
                                    <td class="td_b" colspan="2">Total</td>
                                    <td class="td_b">
                                        @php
                                            $total_qty = DB::table('bk_detail')->where('no_bk',$no_bk)->sum('qty');
                                            echo $total_qty;
                                        @endphp
                                    </td>
                                    <td class="td_b"></td>
                                    <td class="td_b">
                                        @php
                                            $total_harga = DB::table('bk_detail')->where('no_bk', $no_bk)->sum('sub_total');
                                            echo number_format($total_harga, 0, ',', '.');
                                        @endphp
                                    </td>
                                </tr>
                            
                        </tr>
                        @else
                            <tr>
                                <th class="th_b">No. </th>
                                <th class="th_b">Nama Barang</th>
                                <th class="th_b">Banyak</th>
                                @foreach ($bk_detail as $item)
                                @if(isset( $item->keterangan))
                                <th class="th_b">Keterangan</th>
                                @else
                                @endif
                                @endforeach 
                            </tr>
                        
                                @php
                                    $no = 1;
                                @endphp
                                @foreach ($bk_detail as $item)
                                <tr>
                                    <td class="td_b">{{ $no++ }}</td>
                                    <td class="td_b">{{ $item->nama_brg }}</td>
                                    <td class="td_b">{{ $item->qty }} </td>
                                    @if(isset( $item->keterangan))
                                    <td class="td_b">{{ $item->keterangan }} </td>
                                    @else
                                    @endif    
                                    </tr>   
                                @endforeach
                                <tr>
                                    <td class="td_b" colspan="2">Total Harga</td>
                                    <td class="td_b">
                                        @php
                                            $total_qty = DB::table('bk_detail')->where('no_bk',$no_bk)->sum('qty');
                                            echo $total_qty;
                                        @endphp
                                    </td>
                                </tr>
                        @endif
                    </table>
                    {{-- <table class="table_nb">
                        <tr>
                            <td style="width:20px" >Nb:</td>
                            <td>{{$keterangan}}</td>
                        </tr>
                    </table> --}}
                        <div class="row">
                            <div class="col-md-4" align="center">
                                Sopir
                            </div>
                            <div class="col-md-4" align="center">
                                Penerima
                            </div>
                            <div class="col-md-4" align="center">
                                Hormat Kami
                            </div>
                        </div>
                        <br>
                        <br>
                        <br>
                        <div class="row">
                            <div class="col-md-4" align="center">
                                (   {{$sopir}}   )
                            </div>
                            <div class="col-md-4" align="center">
                                (   {{$penerima}}   )
                            </div>
                            <div class="col-md-4" align="center">
                                (   {{$hormat_kami}}   )
                            </div>
                        </div>
                    </table>
                    </table>
              </div>
            </div>
          </div>
        <!-- /page content -->

@endsection

@push('js')
  <script type="text/javascript">
    $(function(){
      window.print();
    }); 
  </script>
@endpush
</body>
</html>