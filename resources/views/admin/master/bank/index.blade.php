@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Bank</h2>
            <ul class="nav navbar-right panel_toolbox">
              
              <a href=" {{ route('bank.form') }} " class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <div class="card-box table-responsive">
                  <table id="tbBank" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>No Rek</th>
                        <th>Bank</th>
                        <th>opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
    // console.log('uyeee');
    var datatableBank = $('#tbBank').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type: 'GET',
            url: '{{ route('bank.datatable') }}', 
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'no_rek', name: 'no_rek' },
          { data: 'nama', name: 'nama' },
          { data: 'opsi', name: 'opsi' }
        ]
    });

    function delete_bank(id) {
      $.ajax({
        type    : 'POST',
        url     : '{{ route('bank.delete') }}',
        data    : { '_idBank' : id },
        success : function (e) {
          notif(e.response.code, e.response.msg);
          datatableBank.draw(); 
        }
      });
    }

  </script>
@endpush