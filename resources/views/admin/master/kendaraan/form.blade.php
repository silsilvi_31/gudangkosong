@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Form Kendaraan</h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              <form id="" action="{{ route('kendaraan.save') }}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
                {{ csrf_field() }}
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Kendaraan <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="" name="kendaraan" required="required" class="form-control ">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No Plat <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="" name="no_plat" required="required" class="form-control ">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">KIR <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="date" id="" name="kir" class="form-control ">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Tahunan <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="date" id="" name="tahunan" class="form-control ">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">5 tahunan <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="date" id="" name="lima_tahunan" class="form-control ">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">penyusutan <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="number" id="" name="penyusutan" class="form-control ">
                  </div>
                </div>
                <div class="ln_solid"></div>
                <div class="item form-group">
                  <div class="col-md-6 col-sm-6 offset-md-3">
                    <a href="{{ route('kendaraan.index') }}" class="btn btn-warning" type="button">Cancel</a>
                    <button class="btn btn-danger" type="reset">Reset</button>
                    <button type="submit" class="btn btn-success">Submit</button>
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>        
@endsection

@push('js')
  <script type="text/javascript">
    console.log('form');
    // var datatablePresensi = $('#tbJabatan').DataTable({
    //     processing: true,
    //     serverSide: true,
    //     ajax: {
    //         type: 'GET',
    //         url: '{{ route('jabatan.datatable') }}', 
    //     },
    //     columns: [
    //       { data: 'DT_RowIndex', name: 'DT_RowIndex' },
    //       { data: 'jabatan', name: 'jabatan' },
    //       { data: 'opsi', name: 'opsi' }
    //     ]
    // });

  </script>
@endpush