<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('data-presensi', 'RestApiController@data_presensi');
Route::post('karyawan-mesin', 'RestApiController@post_karyawan_mesin');
Route::post('presensi', 'RestApiController@post_presensi');

Route::group(['prefix' => 'data'], function () {
    Route::post('login', 'RestApiController@login_android');
    Route::post('masuk', 'RestApiController@absen_masuk');
    Route::post('pulang', 'RestApiController@absen_pulang');
    Route::post('profile', 'RestApiController@profile');
    Route::post('presensi', 'RestApiController@presensi_karyawan');
    // Route::get('/', 'AdminController@shift')->name('shift.index');
    // Route::get('datatable', 'AdminController@shift_datatable')->name('shift.datatable');
    // Route::post('save', 'AdminController@save_shift')->name('shift.save');
    // Route::get('delete/{id}', 'AdminController@delete_shift')->name('shift.delete');            
});




